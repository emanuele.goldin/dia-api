# Elastic Client

## Run Docker container

```bash
docker run --rm -p 8080:8080 \
    -e ELASTICSEARCH_USERNAME='<username>' \
    -e ELASTICSEARCH_PASSWORD='<password>' \
    -e ELASTICSEARCH_URL='<url_1,url_2,...,url_n>' \
    -e CORS_ORIGIN='<url>' \
    ghandin/es_api:1.0
```

## Generate typescript client

```bash
# Install openapi code generator
npm install openapi-typescript-codegen --save-dev

# From project root directory
npx openapi-typescript-codegen --input ./openapi.yaml --output ./generated
```

## TODOs

- [x] Use template for request
- [ ] Add search by topic endpoint
- [x] Implement logger
