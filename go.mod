module api

go 1.19

require github.com/elastic/go-elasticsearch/v8 v8.5.0

require (
	github.com/elastic/elastic-transport-go/v8 v8.0.0-20211216131617-bbee439d559c // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
)
