FROM golang:1.19.5-alpine3.17
WORKDIR /usr/src/app
COPY go.* ./
RUN go mod download
COPY ./ ./
RUN go build -o /usr/local/bin/app
EXPOSE 8080
CMD ["app"]