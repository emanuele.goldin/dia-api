package server

import (
	ec "api/elasticclient"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	cache "github.com/patrickmn/go-cache"
)

const (
	getCountriesEndpoint = "/getCountries"
	getAllEndpoint       = "/getAll"
	getByTimeEndpoint    = "/getByTime"
	getByTopicEndpoint   = "/getByTopic"
	corsOrigin           = "CORS_ORIGN"
	PREFIX               = "Server"
	usage                = `
	API to interface with Elasticsearch

	Endpoints:
		getAll 			- retrieve all tweets
		getByTime 		- retrieve all tweets posted from a specific point in time to another provided in the request
		getCountries	- retrieve all tweets posted in a specific country
		getByTopic		- retrieve all tweets which contain a given word
		`
)

var (
	client *ec.EcClient
	cache_ *cache.Cache
	origin string
)

type (
	rootHandler func(http.ResponseWriter, *http.Request) error
)

func (fn rootHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := fn(w, r)
	if err == nil {
		return
	}

	log.Printf("An error occurred: %v\n%s", err, err.Error())
}

func endpointHandler(w http.ResponseWriter, r *http.Request) error {
	switch r.Method {
	case http.MethodOptions:
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusNoContent)
		return nil
	case http.MethodPost:
		w.Header().Set("Access-Control-Allow-Origin", origin)
		switch r.URL.Path {
		case getAllEndpoint:
			return getAll(w, r)
		case getByTimeEndpoint:
			return getByTime(w, r)
		case getCountriesEndpoint:
			return getCountries(w, r)
		case getByTopicEndpoint:
			return getByTopic(w, r)
		default:
			return Usage(w, r)
		}
	default:
		return fmt.Errorf("unrecognize method %s", r.Method)
	}
}

func init() {
	client = ec.NewEcClient()
	cache_ = cache.New(3*time.Minute, 5*time.Minute)
	if _, ok := os.LookupEnv(corsOrigin); ok {
		origin = os.Getenv(corsOrigin)
	} else {
		origin = "*"
	}
}

func Usage(w http.ResponseWriter, r *http.Request) error {
	w.Write([]byte(usage))
	return nil
}

func getAll(w http.ResponseWriter, r *http.Request) error {
	key := r.URL.Path
	var results ec.GetAllResponse
	if cached, found := cache_.Get(key); found {
		results = cached.(ec.GetAllResponse)
		json.NewEncoder(w).Encode(results.ExtractCleanedResult())
	} else {
		results, err := client.GetAll()
		if err != nil {
			return err
		}
		json.NewEncoder(w).Encode(results.ExtractCleanedResult())
		cache_.Set(key, results, cache.DefaultExpiration)
	}
	return nil
}

func getByTime(w http.ResponseWriter, r *http.Request) (err error) {
	body := readRequestBody[ec.RequestByTime](r)

	key := body.GenerateCacheKey()

	var results ec.TimeResponse

	if cached, found := cache_.Get(key); found {
		results = cached.(ec.TimeResponse)
	} else {
		results, err = client.SearchByTimeframe(body, "")
		if err != nil {
			return err
		}
		if len(results.Aggregations.Countries.Buckets) < ec.ChunkSize {
			cache_.Set(key, results, cache.DefaultExpiration)
			json.NewEncoder(w).Encode(results.ExtractCleanedResult())
			return nil
		} else {
			var acc ec.TimeResponse
			for len(results.Aggregations.Countries.Buckets) > 0 {
				acc.Aggregations.Countries.Buckets = append(acc.Aggregations.Countries.Buckets, results.Aggregations.Countries.Buckets...)
				last_key := results.Aggregations.Countries.AfterKey.Country
				results, err = client.SearchByTimeframe(body, last_key)
				if err != nil {
					return err
				}
			}
			cache_.Set(key, acc, 2*cache.DefaultExpiration)
			json.NewEncoder(w).Encode(acc.ExtractCleanedResult())
			return nil
		}
	}
	json.NewEncoder(w).Encode(results.ExtractCleanedResult())
	return nil
}

func getCountries(w http.ResponseWriter, r *http.Request) (err error) {
	body := readRequestBody[ec.RequestByCountry](r)

	key := body.GenerateCacheKey()

	var results ec.CountryResponse
	if cached, found := cache_.Get(key); found {
		results = cached.(ec.CountryResponse)
	} else {
		results, err = client.SearchByCountry(body, "")
		if err != nil {
			return
		}
		if len(results.Aggregations.Places.Buckets) < ec.ChunkSize {
			cache_.Set(key, results, cache.DefaultExpiration)
			json.NewEncoder(w).Encode(results.ExtractCleanedResult())
			return
		} else {
			var acc ec.CountryResponse
			for len(results.Aggregations.Places.Buckets) > 0 {
				acc.Aggregations.Places.Buckets = append(acc.Aggregations.Places.Buckets, results.Aggregations.Places.Buckets...)
				last_key := results.Aggregations.Places.AfterKey.Place
				results, err = client.SearchByCountry(body, last_key)
				if err != nil {
					return
				}
				json.NewEncoder(w).Encode(results.ExtractCleanedResult())
			}
			cache_.Set(key, acc, cache.DefaultExpiration)
			json.NewEncoder(w).Encode(acc.ExtractCleanedResult())
			return
		}
	}
	json.NewEncoder(w).Encode(results.ExtractCleanedResult())
	return
}

func getByTopic(w http.ResponseWriter, r *http.Request) (err error) {
	body := readRequestBody[ec.RequestByTopic](r)

	key := body.GenerateCacheKey()

	var results ec.TopicResponse

	if cached, found := cache_.Get(key); found {
		results = cached.(ec.TopicResponse)
	} else {
		results, err = client.SearchByTopic(body, "")
		json.NewEncoder(w).Encode(results.ExtractCleanedResultWithText())
		if err != nil {
			return err
		}
		if len(results.Aggregations.Countries.Buckets) < ec.ChunkSize {
			cache_.Set(key, results, cache.DefaultExpiration)
			return nil
		} else {
			var acc ec.TopicResponse
			for len(results.Aggregations.Countries.Buckets) > 0 {
				acc.Aggregations.Countries.Buckets = append(acc.Aggregations.Countries.Buckets, results.Aggregations.Countries.Buckets...)
				last_key := results.Aggregations.Countries.AfterKey.Country
				results, err = client.SearchByTopic(body, last_key)
				if err != nil {
					return err
				}
			}
			cache_.Set(key, acc, 2*cache.DefaultExpiration)
			json.NewEncoder(w).Encode(results.ExtractCleanedResultWithText())
			return nil
		}
	}
	json.NewEncoder(w).Encode(results.ExtractCleanedResultWithText())
	return nil
}

func readRequestBody[B ec.Request](r *http.Request) B {
	defer r.Body.Close()
	var body B
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		log.Default().Printf("%s Unable to decode request body %s. \n error: %s", PREFIX, r.Body, err.Error())
	}
	return body
}
