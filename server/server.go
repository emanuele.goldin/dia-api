package server

import (
	"fmt"
	"net/http"
	"os"
)

const (
	ApiPort = "PORT"
)

var (
	mux  *http.ServeMux
	PORT string
)

func init() {
	mux = http.NewServeMux()
	registerRoutes(mux)
	if _, ok := os.LookupEnv(ApiPort); ok {
		PORT = fmt.Sprintf(":%s", os.Getenv(ApiPort))
	} else {
		PORT = ":8080"
	}
}

func Start() {

	server := http.Server{
		Addr:    PORT,
		Handler: mux,
	}

	if err := server.ListenAndServe(); err != nil {
		panic("Server error.\n" + err.Error())
	}
}
