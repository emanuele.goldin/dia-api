package elastic

import (
	"bytes"
	"fmt"
	"text/template"
	"time"
)

type (
	Tweet struct {
		Text       string   `json:"text,omitempty"`
		Lang       string   `json:"lang,omitempty"`
		Created_at MyTime   `json:"created_at,omitempty"`
		Retweets   int      `json:"retweets,omitempty"`
		Comments   int      `json:"comments,omitempty"`
		Likes      int      `json:"likes,omitempty"`
		Label      string   `json:"label,omitempty"`
		Score      float64  `json:"score,omitempty"`
		Country    string   `json:"country,omitempty"`
		Place      string   `json:"place,omitempty"`
		Location   GeoPoint `json:"location,omitempty"`
	}

	GeoPoint struct {
		Latitude  float64 `json:"lat,omitempty"`
		Longitude float64 `json:"lon,omitempty"`
	}
	Request interface {
		RequestByTime | RequestByCountry | RequestByTopic
		GenerateCacheKey() string
	}
	RequestByTime struct {
		From MyTime `json:"from,omitempty"`
		To   MyTime `json:"to,omitempty"`
	}
	RequestByCountry struct {
		Place string `json:"place,omitempty"`
	}

	RequestByTopic struct {
		Country, Topic string
	}

	Response interface {
		TimeResponse | CountryResponse
	}

	TimeResponse struct {
		Took     float64
		TimedOut bool `json:"timed_out,omitempty"`
		Hits     struct {
			Total struct {
				Value int
			}
		}
		Aggregations struct {
			Countries CountryAggr `json:"countries,omitempty"`
		} `json:"aggregations,omitempty"`
	}

	CountryAggr struct {
		AfterKey struct {
			Country string `json:"country,omitempty"`
		} `json:"after_key,omitempty"`
		Buckets []struct {
			Country struct {
				Code string `json:"country,omitempty"`
			} `json:"key,omitempty"`
			Count     float64 `json:"doc_count,omitempty"`
			Sentiment struct {
				Buckets []struct {
					Label string  `json:"key,omitempty"`
					Count float64 `json:"doc_count,omitempty"`
				} `json:"buckets,omitempty"`
			} `json:"sentiment,omitempty"`
		} `json:"buckets,omitempty"`
	}

	CountryResponse struct {
		Took         float64
		TimedOut     bool `json:"timed_out,omitempty"`
		Aggregations struct {
			Places PlaceAggr `json:"places,omitempty"`
		} `json:"aggregations,omitempty"`
	}

	PlaceAggr struct {
		AfterKey struct {
			Place string `json:"place,omitempty"`
		} `json:"after_key,omitempty"`
		Buckets []struct {
			Place struct {
				Name string `json:"place,omitempty"`
			} `json:"key,omitempty"`
			Count     float64 `json:"doc_count,omitempty"`
			Sentiment struct {
				Buckets []struct {
					Label string  `json:"key,omitempty"`
					Count float64 `json:"doc_count,omitempty"`
				}
			}
		}
	}

	TopicResponse struct {
		Took     float64
		TimedOut bool `json:"timed_out,omitempty"`
		Hits     struct {
			Total struct {
				Value float64
			}
			Hits []struct {
				Source struct {
					Text  string
					Score float64
				} `json:"_source,omitempty"`
			}
		}
		Aggregations struct {
			Countries CountryAggr `json:"countries,omitempty"`
		} `json:"aggregations,omitempty"`
	}

	GetAllResponse struct {
		Took         float64
		TimedOut     bool `json:"timed_out,omitempty"`
		Aggregations struct {
			Countries struct {
				Buckets []struct {
					Country   string  `json:"key,omitempty"`
					Count     float64 `json:"doc_count,omitempty"`
					Sentiment struct {
						Buckets []struct {
							Label string  `json:"key,omitempty"`
							Count float64 `json:"doc_count,omitempty"`
						} `json:"buckets,omitempty"`
					} `json:"sentiment,omitempty"`
				} `json:"buckets,omitempty"`
			}
		}
	}

	cleanedRes map[string]map[string]float64

	cleanedResWithText struct {
		CleanResult cleanedRes
		Texts       []struct {
			Text  string
			Score float64
		}
	}
)

type MyTime struct {
	time.Time
}

func (m *MyTime) UnmarshalJSON(data []byte) error {
	if string(data) == "null" || string(data) == `""` {
		return nil
	}

	tt, err := time.Parse(`"`+time.RFC3339+`"`, string(data))
	if err != nil {
		tt, err = time.Parse(`"2006-01-02"`, string(data))
	}
	*m = MyTime{tt}
	return err
}

func (m *MyTime) convertFormat() string {
	//"2022-12-06T18:08:39.000Z"
	return m.Format("2006-01-02T00:00:00.000Z")
}

func (res *TimeResponse) ExtractCleanedResult() cleanedRes {
	cleaned := cleanedRes{}
	for _, country := range res.Aggregations.Countries.Buckets {
		sentiment := make(map[string]float64)
		for _, sentimentArr := range country.Sentiment.Buckets {
			sentiment[sentimentArr.Label] = sentimentArr.Count
		}
		cleaned[country.Country.Code] = sentiment
	}
	return cleaned
}

func (res *GetAllResponse) ExtractCleanedResult() cleanedRes {
	cleaned := cleanedRes{}
	for _, country := range res.Aggregations.Countries.Buckets {
		sentiment := make(map[string]float64)
		for _, sentimentArr := range country.Sentiment.Buckets {
			sentiment[sentimentArr.Label] = sentimentArr.Count
		}
		cleaned[country.Country] = sentiment
	}
	return cleaned
}

func (res *CountryResponse) ExtractCleanedResult() cleanedRes {
	cleaned := cleanedRes{}
	for _, place := range res.Aggregations.Places.Buckets {
		sentiment := make(map[string]float64)
		for _, sentimentArr := range place.Sentiment.Buckets {
			sentiment[sentimentArr.Label] = sentimentArr.Count
		}
		cleaned[place.Place.Name] = sentiment
	}
	return cleaned
}

func (res *TopicResponse) ExtractCleanedResult() cleanedRes {
	cleaned := cleanedRes{}
	for _, place := range res.Aggregations.Countries.Buckets {
		sentiment := make(map[string]float64)
		for _, sentimentArr := range place.Sentiment.Buckets {
			sentiment[sentimentArr.Label] = sentimentArr.Count
		}
		cleaned[place.Country.Code] = sentiment
	}
	return cleaned
}

func (res *TopicResponse) ExtractCleanedResultWithText() cleanedResWithText {
	cleaned := cleanedResWithText{
		CleanResult: cleanedRes{},
		Texts: []struct {
			Text  string
			Score float64
		}{},
	}
	for _, place := range res.Aggregations.Countries.Buckets {
		sentiment := make(map[string]float64)
		for _, sentimentArr := range place.Sentiment.Buckets {
			sentiment[sentimentArr.Label] = sentimentArr.Count
		}
		cleaned.CleanResult[place.Country.Code] = sentiment
	}
	for _, element := range res.Hits.Hits {
		cleaned.Texts = append(cleaned.Texts, element.Source)
	}
	return cleaned
}

func (req RequestByTime) GenerateCacheKey() string {
	return fmt.Sprintf("%d%d%d-%d%d%d", req.From.Day(), req.From.Month(), req.From.Year(),
		req.To.Day(), req.To.Month(), req.To.Year())
}

func (req RequestByCountry) GenerateCacheKey() string {
	return req.Place
}

func (req RequestByTopic) GenerateCacheKey() string {
	return fmt.Sprintf("%s-%s", req.Country, req.Topic)
}

const timeRequestTemplate = `{
	"query": {
	  "bool":{
		"must":[
		  {
			"range": {
			  "created_at":{
				"gte" : "{{.From}}",
				"lt" : "{{.To}}"
				}
			}
		  }
		]
	  }
	},
	"aggs":{
	  "countries":{
		"composite":{
		  "size": {{.Size}},
		  "sources":[
			{
			  "country":{"terms":{"field" : "country"}}
			}
		  ]{{if .AfterKey}},
		  "after": { "country": "{{.AfterKey}}" }
		  {{- end}}
		},
		"aggs":{
		  "sentiment": {"terms":{"field":"label"}}
		}
	  }
	}
  }`

const countryRequestTemplate = `{
	"query": {
		"bool":{
			"must":[
				{"term" : {"country" : "{{.Country}}"} }
			]
		}
	},
	"aggs":{
		"places":{
			"composite":{
				"size": {{.Size}},
				"sources":[
					{
						"place":{"terms":{"field" : "place"}}
					}
				]{{if .AfterKey}},
				"after": { "place": "{{.AfterKey}}" }
				{{- end}}
			},
			"aggs":{
				"sentiment":{
					"terms":{"field":"label"}
				}
			}
		}
	}
  }`

func GenerateTimeRequestBody(from string, to string, afterKey string) string {
	var doc bytes.Buffer
	data := struct {
		From, To, AfterKey string
		Size               int
	}{
		From:     from,
		To:       to,
		AfterKey: afterKey,
		Size:     ChunkSize,
	}
	t := template.Must(template.New("timeRequest").Parse(timeRequestTemplate))
	t.Execute(&doc, data)
	return doc.String()
}

func GenerateCountryRequestBody(country string, afterKey string) string {
	var doc bytes.Buffer
	data := struct {
		Country, AfterKey string
		Size              int
	}{
		Country:  country,
		AfterKey: afterKey,
		Size:     ChunkSize,
	}
	t := template.Must(template.New("countryRequest").Parse(countryRequestTemplate))
	t.Execute(&doc, data)
	return doc.String()
}

const topicRequestTemplate = `{
	"query": {
		"query_string" : {
			"query" : "{{.Topic}}",
			"fields"  : ["text"]
		  }
	},
	"aggs":{
		"countries":{
			"composite":{
			  "size": {{.Size}},
			  "sources":[
				{
				  "country":{"terms":{"field" : "country"}}
				}
			  ]{{if .AfterKey}},
			  "after": { "country": "{{.AfterKey}}" }
			  {{- end}}
			},
			"aggs":{
			  "sentiment": {"terms":{"field":"label"}}
			}
		}
	}
  }`

func GenerateTopicRequestBody(topic string, country string, afterKey string) string {
	var doc bytes.Buffer
	data := struct {
		Topic, Country, AfterKey string
		Size                     int
	}{
		Topic:    topic,
		Country:  country,
		AfterKey: afterKey,
		Size:     ChunkSize,
	}
	t := template.Must(template.New("topicRequest").Parse(topicRequestTemplate))
	t.Execute(&doc, data)
	return doc.String()
}
