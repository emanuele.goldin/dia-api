package elastic

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/elastic/elastic-transport-go/v8/elastictransport"
	es "github.com/elastic/go-elasticsearch/v8"
)

const (
	elasticUsername = "ELASTICSEARCH_USERNAME"
	elasticPassword = "ELASTICSEARCH_PASSWORD"
	elasticURLs     = "ELASTICSEARCH_URL"
	ChunkSize       = 100
)

var (
	env = []string{elasticUsername, elasticPassword, elasticURLs}
)

type (
	EcClient struct {
		client *es.Client
	}
)

func NewEcClient() *EcClient {
	cert, err := os.ReadFile("ca.pem")

	if err != nil {
		panic("Unable to parse certificate.")
	}

	if err = verifyEnv(); err != nil {
		panic(err.Error())
	}

	username := os.Getenv(elasticUsername)
	password := os.Getenv(elasticPassword)

	client, err := es.NewClient(es.Config{
		Username: username,
		Password: password,
		CACert:   cert,
		Logger: &elastictransport.ColorLogger{
			Output:             os.Stdout,
			EnableRequestBody:  true,
			EnableResponseBody: false},
		DiscoverNodesOnStart:  true,
		DiscoverNodesInterval: 5 * time.Minute,
	})
	if err != nil {
		panic(fmt.Sprintf("Unable to connect to elastic: %v\n%s", err, err.Error()))
	}
	client.Info()
	return &EcClient{
		client: client,
	}
}

func verifyEnv() error {
	var missing []string
	for _, elem := range env {
		if _, ok := os.LookupEnv(elem); !ok {
			missing = append(missing, elem)
		}
	}
	if len(missing) > 0 {
		return errors.New(generateEnvErrorMessage(missing))
	}
	return nil
}

func generateEnvErrorMessage(list []string) string {
	var sb strings.Builder
	length := len(list)
	if length == 1 {
		sb.WriteString("Environment variable " + list[0] + " not found.")
	} else {
		sb.WriteString("Environment variables ")
		for index, elem := range list {
			sb.WriteString(elem)
			if index != length-1 {
				sb.WriteString(", ")
			}
		}
		sb.WriteString(" not found.")
	}
	return sb.String()
}

func (ec *EcClient) SearchByTimeframe(body RequestByTime, afterKey string) (TimeResponse, error) {
	from := body.From
	to := body.To
	res, err := ec.client.Search(
		ec.client.Search.WithIndex("tweets"),
		ec.client.Search.WithSize(0),
		ec.client.Search.WithDocvalueFields("label", "country", "created_at"),
		ec.client.Search.WithSource("false"),
		ec.client.Search.WithBody(strings.NewReader(GenerateTimeRequestBody(from.convertFormat(), to.convertFormat(), afterKey))),
		ec.client.Search.WithPretty(),
	)
	defer res.Body.Close()
	if err != nil {
		return TimeResponse{}, errors.New("Error receiving the response from elastic.\n" + err.Error())
	}
	var resJson TimeResponse
	err = json.NewDecoder(res.Body).Decode(&resJson)
	if err != nil {
		return TimeResponse{}, fmt.Errorf("error parsing response into json : %v\n%s", err, err.Error())
	}
	return resJson, nil
}

func (ec *EcClient) SearchByCountry(body RequestByCountry, afterKey string) (CountryResponse, error) {
	country := body.Place
	res, err := ec.client.Search(
		ec.client.Search.WithIndex("tweets"),
		ec.client.Search.WithSize(0),
		ec.client.Search.WithDocvalueFields("label", "created_at", "place"),
		ec.client.Search.WithSource("false"),
		ec.client.Search.WithBody(strings.NewReader(GenerateCountryRequestBody(country, afterKey))),
		ec.client.Search.WithPretty(),
	)
	if err != nil {
		return CountryResponse{}, fmt.Errorf("error receiving the response from elastic\n%s", err.Error())
	}
	var resJson CountryResponse
	err = json.NewDecoder(res.Body).Decode(&resJson)
	if err != nil {
		return CountryResponse{}, fmt.Errorf("error parsing response into json : %v\n%s", err, err.Error())
	}
	return resJson, nil
}

func (ec *EcClient) GetAll() (GetAllResponse, error) {
	res, err := ec.client.Search(
		ec.client.Search.WithIndex("tweets"),
		ec.client.Search.WithSize(0),
		ec.client.Search.WithDocvalueFields("label", "country", "created_at"),
		ec.client.Search.WithSource("false"),
		ec.client.Search.WithBody(strings.NewReader(`{
			"query": {
				"match_all":{}
			},
			"aggs":{
				"countries":{
					"terms":{
						"field" : "country",
						"size" : 200
					},
					"aggs":{
						"sentiment": {
							"terms":{"field":"label"}
						}
					}
				}
			}
		  }`)),
		ec.client.Search.WithPretty(),
	)
	defer res.Body.Close()
	if err != nil {
		return GetAllResponse{}, fmt.Errorf("error receiving the response from elastic")
	}
	if res.IsError() {
		return GetAllResponse{}, fmt.Errorf("elastic error: \n%s", res.String())
	}
	var resJson GetAllResponse
	err = json.NewDecoder(res.Body).Decode(&resJson)
	if err != nil {
		return GetAllResponse{}, fmt.Errorf("error parsing response into json : %v\n%s", err, err.Error())
	}
	return resJson, nil
}

func (ec *EcClient) SearchByTopic(body RequestByTopic, afterKey string) (TopicResponse, error) {
	size := 0
	source := []string{"text"}
	// retrieve only first 100 tweets
	if afterKey == "" {
		size = ChunkSize
		source = []string{"text", "score", "place"}
	}
	res, err := ec.client.Search(
		ec.client.Search.WithIndex("tweets"),
		ec.client.Search.WithSize(size),
		ec.client.Search.WithDocvalueFields("label", "created_at", "place"),
		ec.client.Search.WithSource(source...),
		ec.client.Search.WithBody(strings.NewReader(GenerateTopicRequestBody(body.Topic, body.Country, afterKey))),
		ec.client.Search.WithPretty(),
	)
	if err != nil {
		return TopicResponse{}, fmt.Errorf("error receiving the response from elastic\n%s", err.Error())
	}
	var resJson TopicResponse
	err = json.NewDecoder(res.Body).Decode(&resJson)
	if err != nil {
		return TopicResponse{}, fmt.Errorf("error parsing response into json : %v\n%s", err, err.Error())
	}
	return resJson, nil
}
